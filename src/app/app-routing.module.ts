import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path:"", loadChildren: () => import('./home/home.module').then(x => x.HomeModule)},
  { path:"search", loadChildren: () => import('./search-result-and-cart/search-result-and-cart.module').then(x => x.SearchResultAndCartModule)},
  { path: "cart", loadChildren: () => import('./cart/cart.module').then(x => x.CartModule)}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
