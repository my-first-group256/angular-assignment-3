import { Component, Input, OnChanges, OnInit, SimpleChange, SimpleChanges } from '@angular/core';
import { CartService } from 'src/app/service/cart.service';
import { FoodService } from 'src/app/service/food.service';

@Component({
  selector: 'app-display-search-result',
  templateUrl: './display-search-result.component.html',
  styleUrls: ['./display-search-result.component.scss']
})
export class DisplaySearchResultComponent implements OnInit, OnChanges {

  @Input()
  citySearchQuery !: string;

  restaurantsByCity: any = [];


  constructor(private api: FoodService, private cartService : CartService) { }

  ngOnChanges(changes: SimpleChanges): void {
    let change = changes['citySearchQuery'];
    console.log(change);
    if (change.currentValue != "") {
      this.getAllDataByCity(change.currentValue);
    } else {
      this.restaurantsByCity = [];
    }
  }

  ngOnInit(): void {
  }

  getAllDataByCity(city: string) {
    this.api.getAllDishesByCity(this.citySearchQuery).subscribe((data) => {
      this.restaurantsByCity = data;
      console.log(this.restaurantsByCity);
    })
  }

  addToCart(item : any) {
    this.cartService.addToCart(item);
  }

}
