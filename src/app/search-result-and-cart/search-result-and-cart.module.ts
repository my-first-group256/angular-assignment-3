import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchResultAndCartRoutingModule } from './search-result-and-cart-routing.module';
import { SearchComponent } from './search/search.component';
import { FormsModule } from '@angular/forms';
import { DisplaySearchResultComponent } from './display-search-result/display-search-result.component';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [
    SearchComponent,
    DisplaySearchResultComponent,
    HeaderComponent
  ],
  imports: [
    CommonModule,
    SearchResultAndCartRoutingModule,
    FormsModule

  ]
})
export class SearchResultAndCartModule { }
