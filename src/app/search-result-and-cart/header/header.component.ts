import { Component, OnInit } from '@angular/core';
import { CartService } from 'src/app/service/cart.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  totalItems : number = 0;

  constructor(private cartService : CartService) { }

  ngOnInit(): void {
    this.getTotalItemsInCart();
  }

  getTotalItemsInCart() {
    this.cartService.getDishList().subscribe((data : any) => {
      this.totalItems = data.length;
    })
  }

}
