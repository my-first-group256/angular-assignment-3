import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SearchComponent } from './search/search.component';
import { DisplaySearchResultComponent } from './display-search-result/display-search-result.component';

const routes: Routes = [
  { path: "", component:SearchComponent},
  { path: "searchResult", component:DisplaySearchResultComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SearchResultAndCartRoutingModule { }
