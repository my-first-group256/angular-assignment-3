import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CartComponent } from './cart/cart.component';
import { CartRoutingModule } from './cart-routing.module';
import { SearchResultAndCartModule } from "../search-result-and-cart/search-result-and-cart.module";



@NgModule({
    declarations: [
        CartComponent
    ],
    imports: [
        CommonModule,
        CartRoutingModule,
        SearchResultAndCartModule
    ]
})
export class CartModule { }
