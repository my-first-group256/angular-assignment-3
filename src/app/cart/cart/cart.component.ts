import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { CartService } from 'src/app/service/cart.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  constructor(private cartService: CartService, private toastr: ToastrService) { }

  cartData: any = [];
  grandTotal: number = 0;
  message !: string;

  ngOnInit(): void {
    this.getCartItems();
  }

  getCartItems() {
    this.cartService.getDishList().subscribe((data: any) => {
      this.cartData = data;
      this.grandTotal = this.cartService.getTotalPrice();
      console.log(this.grandTotal);
    })
    this.getObservableData();
  }

  deleteCartItem(item: any) {
    this.cartService.removeCartItem(item);
  }

  emptyCart() {
    this.cartService.removeAllCartItems();
  }

  getObservableData() {
    if (this.cartData.length > 0) {
      this.cartService.myObservable.subscribe((data: string) => {
        // this.message = data;
        console.log(data);
        this.toastr.success(data);
      }, (error) => {
        this.toastr.error(error);
      }, () => {
        alert('Task Completed!! All the values are emitted!!')
      })
    }
  }

}
