import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  public cartItemList: any = [];
  public dishList = new BehaviorSubject<any>([]);

  constructor() { }

  //upon calling this method getProducts(), data will be emitted/returned from dishList as it is now an observable.
  //Whoever calls this method, can subscribe to it to get the data inside dishList.
  getDishList() {
    return this.dishList.asObservable();
  }

  addToCart(dish: any) {
    this.cartItemList.push(dish);
    this.dishList.next(this.cartItemList);
    this.getTotalPrice();
  }

  getTotalPrice() : number {
    let grandTotal = 0;
    this.cartItemList.map((item : any) => {
      grandTotal += item.price;
    })
    return grandTotal;
  }

  removeCartItem(dish : any) {
    this.cartItemList.map((item : any, index : any) => {
      if(dish.id === item.id) {
        this.cartItemList.splice(index, 1);
      }
    })
    this.dishList.next(this.cartItemList);
  }

  removeAllCartItems() {
    this.cartItemList = [];
    this.dishList.next(this.cartItemList);
  }

  // Observable example
  myObservable = new Observable<string>((observer) => {
    console.log("Observable starts");
    setTimeout(() => { observer.next("Apply CODE: MYFIRSTMEAL")}, 8000);
    setTimeout(() => { observer.next("Let us reward you!!")}, 4000);
    setTimeout(() => { observer.next("What are you waiting for? Order now!!")}, 2000);
    // setTimeout(() => { observer.error(new Error("Something went wrong!"))}, 5000);
    // setTimeout(() => { observer.complete()}, 10000);
  })

}
