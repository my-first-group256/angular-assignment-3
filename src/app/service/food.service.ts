import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class FoodService {

  constructor(private http : HttpClient) { }

  private api_string = "http://localhost:8080/api/dish/";
  private api_string_get_data_by_city = "http://localhost:8080/api/dish/";

  getAllDishesByCity(cityName : string) {
    return this.http.get(this.api_string_get_data_by_city+cityName);
  }
}
